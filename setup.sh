#!/usr/bin/env bash

set -e
set pipefail

CABAL_SANDBOX=${CABAL_SANDBOX:-.cabal-sandbox}

if ! [ -f cabal.sandbox.config ]; then
    echo "Initializing Haskell sandbox in ${CABAL_SANDBOX}"
    cabal sandbox init --sandbox="${CABAL_SANDBOX}" || exit 1
fi

installer_constraints=(
    "liblawless>=0.19.1 && < 0.20"
    "process>=1.4.3.0 && < 1.5"
)
echo "Installing Haskell build dependencies."
cabal install "${installer_constraints[@]}" || exit 1

ghc_flags=(
    -Wall
    -Wno-type-defaults
    -Wno-redundant-constraints
    -threaded
    -feager-blackholing
    -rtsopts
    -with-rtsopts "-N"
)

echo "Running Haskell installer."
cabal exec -- runghc "${ghc_flags[@]}" initialize.hs
