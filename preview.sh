#!/usr/bin/env bash

set -e pipefail

readonly CABAL_SANDBOX=${CABAL_SANDBOX:-.cabal-sandbox}
readonly builder="${CABAL_SANDBOX}/bin/$(awk '$1 == "executable" {print $2}')"

if ! [ -x "${builder}" ]; then
    echo "${builder} not found. Running setup.sh"
    ./setup.sh || exit 1
fi

run () {
    "${builder}" "$@"
}

if ! run build || run rebuild; then
    echo "Could not build site."
    exit 1
fi

run watch
