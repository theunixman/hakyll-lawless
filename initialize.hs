{-# Language NoImplicitPrelude #-}
{-# Language TemplateHaskell #-}
{-# Language ConstraintKinds #-}
{-# Language DefaultSignatures #-}
{-# Language DeriveDataTypeable #-}
{-# Language DeriveGeneric #-}
{-# Language ExtendedDefaultRules #-}
{-# Language FlexibleContexts #-}
{-# Language FlexibleInstances #-}
{-# Language FunctionalDependencies #-}
{-# Language GADTs #-}
{-# Language GeneralizedNewtypeDeriving #-}
{-# Language KindSignatures #-}
{-# Language LambdaCase #-}
{-# Language MultiParamTypeClasses #-}
{-# Language OverloadedStrings #-}
{-# Language PartialTypeSignatures #-}
{-# Language RankNTypes #-}
{-# Language RecordWildCards #-}
{-# Language ScopedTypeVariables #-}
{-# Language StandaloneDeriving #-}
{-# Language TypeFamilies #-}
{-# Language TypeSynonymInstances #-}
{-# Language UnicodeSyntax #-}

import Lawless
import IO
import Path
import Text
import Textual hiding (toString)
import Data.String (IsString(..))
import System.Process

newtype Message = Message Text deriving (Eq, Show, Ord, Printable, IsString)

newtype Executable = Executable RelFile deriving (Eq, Show, Ord)
makePrisms ''Executable

instance Printable Executable where
    print = print ∘ toString ∘ view _Executable

newtype Argument = Argument Text deriving (Eq, Show, Ord, Printable, IsString)
makePrisms ''Argument

data Command = Command {
    _coMessage ∷ Message,
    _coExecutable ∷ Executable,
    _coArguments ∷ [Argument]
    } deriving (Eq, Ord, Show)
makeLenses ''Command

instance Printable Command where
    print (Command {..}) =
        hsep $ concatOf traversed [[print _coMessage, print _coExecutable], over traversed print _coArguments]

executable ∷ Getter Command [Char]
executable = to (\Command{..} → toString $ _coExecutable ^. _Executable)

arguments ∷ Getter Command [[Char]]
arguments = to (\Command{..} → over traversed (view unpacked ∘ view _Argument) _coArguments)

run ∷ Command → IO ()
run cmd = do
    putStrLn $ cmd ^. coMessage
    callProcess (cmd ^. executable) (cmd ^. arguments)

command ∷ Text → Message → [Argument] → Command
command cmd msg = Command msg (Executable $ relFile cmd)

cabal ∷ Message → [Argument] → Command
cabal = command "cabal"

npm ∷ Message → [Argument] → Command
npm = command "npm"

bower ∷ Message → [Argument] → Command
bower = command "./node_modules/.bin/bower"

commands ∷ [Command]
commands = [
           npm "Installing Node packages" ["install"],
           bower "Installing Bower packages" ["install"],
           cabal "Updating Haskell package list" ["update"],
           cabal "Installing Haskell dependencies" ["install", "--only-dependencies"],
           cabal "Installing site builder" ["install"]
           ]

main ∷ IO ()
main =
    mapMOf_ traversed run commands
