#!/usr/bin/env zsh

rsync -rvv --delete-after --progress site/ tartarus.theunixman.com:/var/www/theunixman.com/htdocs/
