# Site Template

This is a web site front-end template using:

- [Foundation 6][foundation]
- [PureScript 0.10.7][PureScript]
- [Halogen 0.12.0][halogen]
- [Hakyll][hakyll]

[halogen]: https://pursuit.purescript.org/packages/purescript-halogen/0.12.0 "halogen"
[hakyll]: https://jaspervdj.be/hakyll/ "Hakyll"
[PureScript]: http://www.purescript.org/ "purescript"
[foundation]: http://foundation.zurb.com/sites/docs/ "Foundation 6"

## Code Organization

### Source Files

All source files for the site should be put under `src`. The hierarchy
here is:

- `app`: Application code that gets compiled and assembled into the
  final `app.js` bundle. This is then placed under
  `site/assets/js/app.js`.
  - `purs`: PureScript modules
    - `App.purs`: The main entry point for the application.
  - `scss`: SCSS components
- `data`: Data files that get copied to `site/data`
- `layouts`: Templates used by various pages. `default.html` is the
  default template for pages without a specific template specified.
- `pages`: Actual page content.
- `partials`: Snippets of templates that Hakyll can include using the
  `$partial$` macro.
