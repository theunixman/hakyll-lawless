2017-03-11  Evan Cofsky  <evan@theunixman.com>

	* src/partials/libs.html: Include all the third party libs we require.

	* src/partials/head.html: Include the libs.html partial.

	* src/partials/footer.html: Include the app.html partial.

	* src/partials/app.html: Use Main.js instead of app.js.

	* src/hs/Routes.hs (patternRoutes): Build a custom route for any
	file matching a pattern.
	(jsRoutes): Route all JS files to the JS target.
	(fontRoutes): Route all font files to the font target.

	* src/hs/Patterns.hs (jsPattern): Pattern for any ".js" file.
	(libsPattern): Pattern for our external JS libraries.
	(pages): Complement the right pattern so we build pages.
	(fontAwesomePattern): Pattern for FontAwesome fonts.
	(fonts): Include `fontAwesomePattern`

	* src/hs/Paths.hs (fontAwesomeFonts): FontAwesome font path.
	(jquery): jQuery library path
	(motionUI): motion-ui library path
	(jsLibs): all the third-party JS libraries we need
	(jsTarget): the target path for all JS files
	(fontsTarget): the target path for all font files

	* src/hs/Main.hs (main):
	- Use `copyFileCompiler` instead of `assetsCompiler`.
	- Add JS libraries
	- Add FontAwesome fonts

	* src/hs/Compilers.hs : remove `assetsCompiler`. It's just `copyFileCompiler`.
