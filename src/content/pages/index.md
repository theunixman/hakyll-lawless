---
title: Sample Main Page
subtitle: How to Use Foundation
description: How The UNIX Man Works
sidebar-icon: heartbeat
---

## The Landing Page

This is the main page of the site. It uses the layout in
`src/content/layouts/default.html` by default. There are also several
*partials* under `src/content/partials/`. These are snippets of code
that can be included directly in the page.

## Templates

The content of any of these pages under `src/content/pages/` are
inserted into one of the templates under `src/content/layouts/`. If
one isn't specified in the metadata, `default.html` will be used by
default.

A custom template can be specified by setting the `template` parameter
in the page metadata to the path of an HTML file under
`src/content/layouts`, and it'll be used instead.

## Partials

The `src/content/partials/` folder contains snippets of HTML that can
be included piecemeal. They can also use variables from the page
metadata and the page context. These are really useful for sharing
code between templates, between pages, and across all site sections.
