-- | A simple menu navigation gadget using the Foundation menu.

module Navigation.Items where

import Prelude
import Data.Generic.Rep as Rep
import Data.Foreign (ForeignError(..), fail, readString, toForeign)
import Data.Foreign.Class (class AsForeign, class IsForeign)
import Data.Foreign.Generic (defaultOptions, readGeneric, toForeignGeneric)
import Data.Foreign.Generic.Types (SumEncoding(..), Options)
import Data.Foreign.NullOrUndefined (NullOrUndefined)
import Data.Generic.Rep.Show (genericShow)

-- | A menu item fetched from a URL.
newtype Item = Item {
    itemHeading ∷ String,
    itemLink ∷ String
    }

derive instance repGenericItem :: Rep.Generic Item _
derive instance eqItem :: Eq Item
instance showItem :: Show Item where
  show = genericShow
instance isForeignItem :: IsForeign Item where
  read = readGeneric $ defaultOptions {unwrapSingleConstructors = true}
instance asForeignItem :: AsForeign Item where
  write = toForeignGeneric $ defaultOptions {unwrapSingleConstructors = true}
