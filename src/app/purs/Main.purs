module Main where

import Prelude (Unit, bind)
import Control.Monad.Eff (Eff)
import Foundation
import Control.Monad.Eff.Console (logShow,  CONSOLE)
import Navigation

main ∷ ∀ eff. Eff (console ∷ CONSOLE, found ∷ FOUND | eff) Unit
main = foundation
