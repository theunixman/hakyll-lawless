module Foundation where

import Prelude (Unit)
import Control.Monad.Eff (Eff)

foreign import data FOUND ∷ !

foreign import foundation ∷ forall eff. Eff (found ∷ FOUND | eff) Unit
