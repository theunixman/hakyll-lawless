{-# Language TemplateHaskell #-}

module Compilers where

import Hakyll
import Hakyll.Web.Sass
import Lawless hiding (Context, (<.>))
import Path
import Set
import Globs
import Text
import Paths
import System.Process
import Data.ByteString.Lazy (ByteString)

default (Text)

-- * Compilers

-- ** Pages

-- | Publication date format
dateFormat ∷ [Char]
dateFormat = "%A %B %e, %Y at %R"

-- | The context for a page.
pageCtx :: Context [Char]
pageCtx =
    dateField "date" dateFormat `mappend`
    modificationTimeField "modified" dateFormat `mappend`
    modificationTimeField "copyright" "%Y" `mappend`
    defaultContext

-- | Apply a different template for an item if specified. Otherwise
-- used the default $d$.
applyItemTemplate ∷ Identifier → Item [Char] → Compiler (Item [Char])
applyItemTemplate d i = do
    t ← maybe d (view layout ∘ relFile) <$> getMetadataField (itemIdentifier i) "template"
    loadAndApplyTemplate t pageCtx i

-- | Compiles a page from 'Paths.pages'.
--
-- 1. Reads the content.
-- 2. Applies the content as a template to evaluate any macros.
-- 3. Renders the page using 'renderPandoc'.
-- 4. Applies the page template using 'applyItemTemplate'.
pagesCompiler ∷ Identifier → Compiler (Item [Char])
pagesCompiler d =
        getResourceString >>=
        applyAsTemplate pageCtx >>=
        renderPandoc >>=
        applyItemTemplate d

-- | Adds a 'Pattern' of dependencies for pages.
pagesDepends ∷ ∀ a. Pattern → Rules a → Rules a
pagesDepends tps r =
    makePatternDependency tps
        >>= \q → rulesExtraDependencies [q] r

-- ** Styling

-- | Compile SCSS.
compileScss ∷ ∀ (f ∷ * → *). Foldable f ⇒ Maybe RelFile → f RelDir → Compiler (Item [Char])
compileScss m ps =
    fmap compressCss <$> sassCompilerWith (sassOptions m ps)

-- ** SASS compiler options
sassOptions ∷ ∀ (f ∷ * → *). Foldable f ⇒ Maybe RelFile → f RelDir → SassOptions
sassOptions m ps = sassDefConfig {
    sassOutputStyle = SassStyleNested,
    sassIncludePaths = Just $ foldMapOf folded (\p → [toString p]) ps,
    sassSourceMapFile = toString <$> m
    }

-- | SCSS files $app.scss$ depends on.
scssDepends ∷ ∀ a. Set RelDir → RelFile → Rules a → Rules a
scssDepends ps a r =
    let
        p = conjunction $ (complement $ a ^. file ^. glob):(maybeToList $ subDirsExtensions [relFile "scss"] ps)
    in
        makePatternDependency (firstOf traversed p ^. _Just)
        >>= \q → rulesExtraDependencies [q] r

-- ** PureScript

data PulpOption =
    BuildPath RelDir -- ^ The directory for build output
    | SkipEntryPoint -- ^ Don't automatically call Main.
    | DependencyPath RelDir -- ^ The path to dependencies if not $bower_path$
    | Optimise -- ^ Perform dead code elimination
    | SourceMap RelFile -- ^ Write a source map
    deriving (Eq, Ord, Show)

popt ∷ Getter PulpOption [Text]
popt =
    let
        p = \case
            SkipEntryPoint → ["--skip-entry-point"]
            BuildPath r → ["--build-path", toText r]
            DependencyPath d → ["--dependency-path", toText d]
            Optimise → ["--optimise"]
            SourceMap s → ["--source-map", toText s]
    in
        to p

newtype PulpOptions = PulpOptions (Set PulpOption) deriving (Eq, Ord, Show)
makePrisms ''PulpOptions

pulp ∷ Getter PulpOption PulpOptions
pulp = to $ PulpOptions ∘ view sing

popts ∷ Getter PulpOptions [Text]
popts = to $ foldMapOf folded (view popt) ∘ view _PulpOptions

instance Monoid PulpOptions where
    mempty = (BuildPath $ relDir "dist/psc") ^. pulp
    a `mappend` b = PulpOptions $ (a ^. _PulpOptions) ∪ (b ^. _PulpOptions)

pursCompiler ∷ Compiler (Item [Char])
pursCompiler = pursCompilerWith mempty

pursCompilerWith ∷ PulpOptions → Compiler (Item [Char])
pursCompilerWith options = cached "Hakyll.Web.Pandoc.pursCompilerWith" $ do
    ss ← toText ∘ dropFileName ∘ relFile <$> getResourceFilePath
    let o =over traversed (view unpacked) $ (options ^. popts) ++ ["--src-path", ss]
    Item <$> getUnderlying <*> (unsafeCompiler $ readProcess "pulp" ("browserify":o) "")

-- | All the purs and js files $Main.purs$ depends on.
pursDepends ∷ Pattern → Pattern → Rules a → Rules a
pursDepends pss pm r =
    makePatternDependency (complement pm .&&. pss) >>= \q → rulesExtraDependencies [q] r

-- ** Favicon

-- | Compile $favicon.ico$ from $favicon.svg$
favicoCompiler ∷  Compiler (Item ByteString)
favicoCompiler = cached "ImageMagick.favicon.ico" $ do
    let tgts = foldMapOf folded (\t → [toString t]) $ favicos ^. fisTargets
    Item <$> getUnderlying <*> (unixFilterLBS "convert" (concatOf traversed [tgts, ["ico:-"]]) "")
