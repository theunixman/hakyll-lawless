{-|
Module:             Images
Description:        Optimize images for the web and build the Favicons.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Images where

import Hakyll
import Lawless hiding (Context, (<.>))
import Path
import Set
import Globs
import Text
import Paths
import System.Process
import Set

default (Text)

-- * Favicons
-- See @audreyr's brilliant post: <https://github.com/audreyr/favicon-cheat-sheet>

-- | All of the dimensions of favicon PNGs we need.
faviconDimensions ∷ Set Integer
faviconDimensions = setOf traversed [16, 32, 57, 76, 96, 120, 128, 144, 152, 180, 195, 196, 228, 270, 558]

-- | The resolutions that go into the $favicon.ico$ file
icoResolutions ∷ Set Integer
icoResolutions = setOf traversed [16, 24, 32, 48, 64]

