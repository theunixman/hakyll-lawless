{-# Language TemplateHaskell #-}

module Paths where

import Hakyll
import Lawless hiding (Context, (<.>))
import Path
import Text hiding (text)
import Set
import Data.Word
import Textual

default (Text)

-- * Paths

-- ** Targets

assetsT ∷ Getter RelDir RelDir
assetsT = to $ ((relDir "assets") </>)

jsT ∷ RelDir
jsT = (relDir "js") ^. assetsT

fontsT ∷ RelDir
fontsT = (relDir "fonts") ^. assetsT

cssT ∷ RelDir
cssT = (relDir "css") ^. assetsT

filesT ∷ RelDir
filesT = (relDir "files") ^. assetsT

dataT ∷ RelDir
dataT = (relDir "data") ^. assetsT

imagesT ∷ RelDir
imagesT = (relDir "img") ^. assetsT

-- ** Identifiers

-- | 'Iso' between 'Identifier's and 'RelFile's
identifier ∷ Iso' Identifier RelFile
identifier = iso (relFile ∘ toFilePath) (fromFilePath ∘ Path.toString)

-- | 'Getter' for the 'RelFile' of an 'Item'
itemPath ∷ Getter (Item a) RelFile
itemPath = to (view identifier ∘ itemIdentifier)

-- ** Pages, Layouts, and Partials

src ∷ Getter RelDir RelDir
src = to $ (relDir "src" </>)

-- | Content
content ∷ Getter RelDir RelDir
content = to $ (((relDir "content") ^. src) </>)

pages ∷ RelDir
pages = (relDir "pages") ^. content

layouts ∷ RelDir
layouts = (relDir "layouts") ^. content

layoutFile ∷ Getter RelFile RelFile
layoutFile = to $ (layouts </>)

-- | The path to all the partials.
partials ∷ RelDir
partials = (relDir "partials") ^. content

-- *** Static Assets

-- | Root of static assets
assets ∷ Getter RelDir RelDir
assets = to $ (((relDir "assets") ^. src) </>)

-- | Images
images ∷ RelDir
images = (relDir "img") ^. assets

-- | Fonts
fonts ∷ RelDir
fonts = (relDir "fonts") ^. assets

fontAwesomeFonts ∷ RelDir
fontAwesomeFonts = (relDir "font-awesome/fonts") ^. bower

-- | Files
files ∷ RelDir
files = (relDir "files") ^. assets

dataFiles ∷ RelDir
dataFiles = (relDir "data") ^. assets

-- ** JavaScript Modules

nodeModules ∷ RelDir
nodeModules = relDir "node_modules"

bowerComponents ∷ RelDir
bowerComponents = relDir "bower_components"

node ∷ Getter RelDir RelDir
node = to $ (nodeModules </>)

bower ∷ Getter RelDir RelDir
bower = to $ (bowerComponents </>)

jquery ∷ RelFile
jquery = ((relDir "jquery/dist") ^. bower) </> (relFile "jquery.min.js")

motionUI ∷ RelFile
motionUI = ((relDir "motion-ui/dist") ^. bower) </> (relFile "motion-ui.min.js")

foundation ∷ RelFile
foundation = ((relDir "foundation-sites/dist/js") ^. bower) </> (relFile "foundation.min.js")

jsLibs ∷ Set RelFile
jsLibs = setOf traversed [jquery, motionUI, foundation]

-- ** App Sources

-- | App sources
app ∷ Getter RelDir RelDir
app = to $ (((relDir "app") ^. src) </>)

-- | Root of application SCSS
scss ∷ RelDir
scss = (relDir "scss") ^. app

-- | SCSS Search Paths
scssPaths ∷ Set RelDir
scssPaths =
    let
        nodeScss = over traversed (view bower ∘ relDir) [
            "font-awesome/scss",
            "foundation-sites/scss",
            "motion-ui/src"
            ]
    in
        foldMapOf traversed (view sing) (scss:nodeScss)

-- | The actual $app.scss$
scssMain ∷ RelFile
scssMain =scss </> relFile "Main.scss"

cssSrcMap ∷ RelFile
cssSrcMap = cssT </> relFile "Main.css.map"

-- | $src/app/purs$
purs ∷ RelDir
purs = (relDir "purs") ^. app

pursMain ∷ RelFile
pursMain = purs </> relFile "Main.purs"

pursSourceMap ∷ RelFile
pursSourceMap = jsT </> relFile "Main.js.map"

-- | A specific layout 'Identifier'
layout ∷ Getter RelFile Identifier
layout = to $ fromFilePath ∘ Path.toString ∘ view layoutFile

-- | The default layout 'Identifier'.
defaultLayout ∷ Identifier
defaultLayout = (relFile "default.html") ^. layout

-- ** Favicons

faviconSrc ∷ RelFile
faviconSrc = (view src (relDir "img")) </> (relFile "favicon.svg")

favico ∷ RelFile
favico = imagesT </> (relFile "favicon.ico")

data Favicon = Favicon {
    _fisName ∷ RelFile,
    _fisSize ∷ Word16
    } deriving (Eq, Show)
makeLenses ''Favicon

fisTarget ∷ Getter Favicon RelFile
fisTarget = to $ (</>) imagesT ∘ view fisName

fisTargets ∷ Getter (Set Favicon) (Set RelFile)
fisTargets = to $ foldMapOf folded (view sing ∘ view fisTarget)

instance Ord Favicon where
    a `compare` b = (a ^. fisSize) `compare` (b ^. fisSize) ⊕ (a ^. fisName) `compare` (b ^. fisName)

-- | Creates a 'Favicon' with a default 'fisName'.
favicon ∷ Getter Word16 Favicon
favicon =
    to $ (\s → Favicon (relFile $ buildText $ hcat [text "favicon-", print s, text ".png"]) s)

favicons ∷ Set Favicon
favicons = setOf traversed [
    16 ^. favicon,
    24 ^. favicon,
    32 ^. favicon,
    48 ^. favicon,
    64 ^. favicon,
    57 ^. favicon,
    76 ^. favicon,
    96 ^. favicon,
    120 ^. favicon,
    128 ^. favicon,
    128 ^. favicon & fisName .~ (relFile "smalltile.png"),
    152 ^. favicon,
    180 ^. favicon,
    195 ^. favicon,
    195 ^. favicon,
    196 ^. favicon,
    228 ^. favicon,
    270 ^. favicon & fisName .~ (relFile "mediumtile.png"),
    558 ^. favicon & fisName .~ (relFile "largetile.png")
    ]

-- | The images for the $favicon.ico$ itself
favicos ∷ Set Favicon
favicos =
    let
        szs = setOf traversed [16, 32, 48, 24, 64]
        msz = maximumOf folded szs ^?! _Just
    in
        setOf traversed
        $ favicons
        ^.. takingWhile ((>=) msz ∘ view fisSize) (folded ∘ filtered (\i → has _Just $ szs ^.at (i ^. fisSize)))
