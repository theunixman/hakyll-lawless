module Main where

import Hakyll
import Lawless hiding (Context, (<.>))
import Text
import Patterns
import Routes
import Compilers
import qualified Paths as P
import System.Environment

default (Text)

-- * Compiler

main :: IO ()
main = do
    debug ← has _Just <$> lookupEnv "SOURCE_MAPS"
    let pulpOptions =
            mconcat [Optimise ^. pulp, bool ((SourceMap P.pursSourceMap) ^. pulp) mempty debug]
    let cssMapPath = bool Nothing (Just P.cssSrcMap) debug

    hakyllWith siteConfig $ do
        match templates $ do
            compile templateBodyCompiler
        match fontAwesomeFonts $ do
            route fontAwesomeRoutes
            compile copyFileCompiler
        match fonts $ do
            route fontRoutes
            compile copyFileCompiler
        pagesDepends templates <$> match pages $ do
            route pagesRoutes
            compile $ pagesCompiler P.defaultLayout
        match favicon $ do
            route faviconRoutes
            compile copyFileCompiler
        scssDepends P.scssPaths P.scssMain <$> match scssMain $ do
            route scssRoutes
            compile $ compileScss cssMapPath P.scssPaths
        match libsPattern $ do
            route jsRoutes
            compile copyFileCompiler
        pursDepends pursPattern pursMain <$> match pursMain $ do
            route pursRoutes
            compile $ pursCompilerWith pulpOptions
        match assets $ do
            route assetsRoutes
            compile copyFileCompiler

siteConfig ∷ Configuration
siteConfig = defaultConfiguration {
    destinationDirectory = "site"
    }
