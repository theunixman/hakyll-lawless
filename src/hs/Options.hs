module Options where

import Hakyll
import Hakyll.Web.Sass
import Lawless hiding (Context, (<.>))
import Path
import Text
import Set
