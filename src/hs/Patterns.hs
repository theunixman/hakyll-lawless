module Patterns where

import Hakyll
import Lawless hiding (Context, (<.>), (<>))
import Path
import Text
import qualified Paths as P
import Globs
import Set

-- * Patterns

-- ** Libraries

jsPattern ∷ WildCard
jsPattern = (relFile "js") ^. extension

libsPattern ∷ Pattern
libsPattern =
    disjunction' $ foldMapOf folded ((:[]) ∘ view glob ∘ view file) P.jsLibs

-- ** Content

-- | $*.html$
html ∷ RelFile
html = (relFile ".html")

templates ∷ Pattern
templates = subDirsExtensions' [html] [P.layouts, P.partials]

pageExtensions ∷ Set RelFile
pageExtensions = extensions [".html", ".md", ".tex"]

-- | All of the files for pages but not metadata files.
pages ∷ Pattern
pages = subDirsExtensions' pageExtensions [P.pages]

htaccess ∷ Pattern
htaccess =
    let
        f = (relFile ".htaccess") ^. file
        top = (P.pages ^. dir >> f) ^. glob
        rest = (P.pages ^. subDirFiles >> f) ^. glob
    in
        top .||. rest

-- ** Styling
scssMain ∷ Pattern
scssMain = P.scssMain ^. file ^. glob

-- | Pattern for all purs/js files
pursPattern ∷ Pattern
pursPattern =
    let
        bpkgs = (P.bowerComponents ^. subDirFiles >> (relFile "bower.json") ^. file) ^. glob
        npkgs = (P.nodeModules ^. subDirFiles >> (relFile "package.json") ^. file) ^. glob
        psrcs = (P.purs ^. subDirFiles >> (relFile "purs") ^. extension) ^. glob
    in
        disjunction' [bpkgs, npkgs, psrcs]

-- | The location of Main.purs
pursMain ∷ Pattern
pursMain = P.pursMain ^. file ^. glob

-- ** Image files

extensions ∷ ∀ (f ∷ * → *). Foldable f ⇒ f Text → Set RelFile
extensions = foldMapOf folded (view sing ∘ relFile)

-- | Image file extensions
imageExts ∷ Set RelFile
imageExts = extensions ["png", "jpg", "jpeg", "svg", "tif", "tiff", "svgz"]

favicon ∷ Pattern
favicon = (P.images </> relFile "favicon.ico") ^. file ^. glob

images ∷ Pattern
images =
    subDirsExtensions' imageExts [P.images] .&&. (complement favicon)

-- ** Fonts

-- | Font file extensions
fontExts ∷ Set RelFile
fontExts = extensions ["otf", "ttf", "eot", "svg", "woff", "woff2"]

fonts ∷ Pattern
fonts = disjunction' $ foldMapOf folded ((:[]) ∘ dirExtension P.fonts) fontExts

fontAwesomeFonts ∷ Pattern
fontAwesomeFonts = subDirExtensions P.fontAwesomeFonts fontExts ^?! _Just

files ∷ Pattern
files = (P.files ^. subDirFiles) ^. glob

dataFiles ∷ Pattern
dataFiles = (P.dataFiles ^. subDirFiles) ^. glob

assets ∷ Pattern
assets = disjunction' [images, fonts, files, dataFiles]
