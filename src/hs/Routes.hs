module Routes where

import Hakyll
import Path
import Lawless hiding (Context, (<.>))
import Patterns
import qualified Paths as P

-- * Routes

-- ** Routes for our sources.

pagesRoutes ∷ Routes
pagesRoutes = gsubRoute "src/content/pages/" (const "")
    `composeRoutes` setExtension ".html"

faviconRoutes ∷ Routes
faviconRoutes = gsubRoute "src/assets/img/" (const "")

scssRoutes ∷ Routes
scssRoutes = gsubRoute "src/app/scss/" (const "assets/css/")
    `composeRoutes` setExtension ".css"

assetsRoutes ∷ Routes
assetsRoutes = gsubRoute "src/assets/" (const "assets/")

pursRoutes ∷ Routes
pursRoutes = gsubRoute "src/app/purs" (const "assets/js")
    `composeRoutes` setExtension ".js"

-- ** Pattern route helper for third party files.

patternRoutes ∷ Pattern → RelDir → Routes
patternRoutes pat tgt =
    let
        pth = relFile ∘ toFilePath
        bn p = (takeBaseName p) <.> (takeExtension p)
        bf p = tgt </> bn p
        r = toString ∘ bf ∘ pth
    in
        matchRoute pat (customRoute r)

jsRoutes ∷ Routes
jsRoutes = patternRoutes "**.js" P.jsT

fontRoutes ∷ Routes
fontRoutes = patternRoutes fonts P.fontsT

fontAwesomeRoutes ∷ Routes
fontAwesomeRoutes = patternRoutes fontAwesomeFonts P.fontsT

dataRoutes ∷ Routes
dataRoutes = patternRoutes dataFiles P.dataT

filesRoutes ∷ Routes
filesRoutes = patternRoutes files P.filesT
