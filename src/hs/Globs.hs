{-# Language DeriveFunctor #-}

module Globs (
    WildCard,
    glob,
    h,
    k,
    k2,
    file,
    dir,
    dirFiles,
    subDirFiles,
    extension,
    prefix,
    end,
    conjunction,
    disjunction,
    conjunction',
    disjunction',
    dirExtension,
    dirExtensions,
    dirExtensions',
    subDirExtension,
    subDirExtensions,
    subDirsExtensions,
    subDirsExtensions'
    ) where

import Lawless
import Text
import Path
import Control.Monad.Free
import Hakyll

default (Text)

data WildCardS w n =
    D w n -- ^ A directory component.
    | K n -- ^ All matches within a directory ($*$)
    | K2 n -- ^ All intervening matches including directories ($**$)
    | G w n -- ^ A glob followed by a match ($*<m>$).
    | H w n -- ^ A match followed by a glob ($<m>*$).
    | F w -- ^ A file name. This is a terminal case.
    | T -- ^ An empty terminal case, for ending a pattern.
    deriving (Eq, Ord, Show, Functor)

type WildCard = Free (WildCardS Text) ()

dirSep ∷ Text
dirSep = "/"

d ∷ RelDir → WildCard
d r = liftF (D (toText r) ())

k ∷ WildCard
k = liftF (K ())

k2 ∷ WildCard
k2= liftF (K2 ())

g ∷ RelFile → WildCard
g r = liftF (G (toText r) ())

h ∷ RelFile → WildCard
h r = liftF (H (toText r) ())

f ∷ RelFile → WildCard
f r = liftF (F (toText r))

end ∷ WildCard
end = liftF T

glob' ∷ Getter WildCard Text
glob' = to $ \case
    Free (D r n) → r <> dirSep <> n ^. glob'
    Free (K n) → "*" <> dirSep <> n ^. glob'
    Free (K2 n) → "**" <> dirSep <> n ^. glob'
    Free (G r n) → "*" <> r <> n ^. glob'
    Free (H r n) → r <> "*" <> n ^. glob'
    Free (F n) → n
    Free T → ""
    Pure _ → ""

-- | Convert a 'WildCard' to a 'Pattern'
glob ∷ Getter WildCard Pattern
glob = to $ fromGlob ∘ view unpacked ∘ view glob'

dir ∷ Getter RelDir WildCard
dir = to d

file ∷ Getter RelFile WildCard
file = to f

-- | Getter for all the files in a directory
dirFiles ∷ Getter RelDir WildCard
dirFiles = to (\rd → rd ^. dir >> k)

-- | Getter for all the files under a directory
subDirFiles ∷ Getter RelDir WildCard
subDirFiles = to (\rd → rd ^. dir >> k2)

-- | Getter for an 'Extension'
extension ∷ Getter RelFile WildCard
extension = to g

prefix ∷ Getter RelFile WildCard
prefix = to g

-- | Fold of 'Patterns' with an operation.
foldWildCard ∷ ∀ (f ∷ * → *). Foldable f ⇒ (Pattern → Pattern → Pattern) → f Pattern → Maybe Pattern
foldWildCard fn ps =
    firstOf folded ps >>= \i → return $ foldlOf (dropping 1 folded) fn i ps

-- | Disjunction of 'WildCard' patterns
disjunction ∷ ∀ (f ∷ * → *). Foldable f ⇒ f Pattern → Maybe Pattern
disjunction = foldWildCard (.||.)

disjunction' ∷ ∀ (f ∷ * → *). Foldable f ⇒ f Pattern → Pattern
disjunction' ps = disjunction ps ^?! _Just

conjunction ∷ ∀ (f ∷ * → *). Foldable f ⇒ f Pattern → Maybe Pattern
conjunction = foldWildCard (.&&.)

conjunction' ∷ ∀ (f ∷ * → *). Foldable f ⇒ f Pattern → Pattern
conjunction' ps = conjunction ps ^?! _Just

-- ** Products of directories and extensions

-- | All files with an extension under a directory
dirExtension ∷ RelDir → RelFile → Pattern
dirExtension r e = view glob $ (r ^. dir) >> (e ^. extension)

dirExtensions ∷ ∀ (f ∷ * → *). Foldable f ⇒ RelDir → f RelFile → Maybe Pattern
dirExtensions r es = disjunction $ foldMapOf folded ((:[]) ∘ dirExtension r) es

dirExtensions'  ∷ ∀ (f ∷ * → *). Foldable f ⇒ RelDir → f RelFile → Pattern
dirExtensions' r es = dirExtensions r es ^?! _Just

-- | All files with an extension under a all children of a directory.
subDirExtension ∷ RelDir → RelFile → Pattern
subDirExtension r e = view glob $ (r ^. subDirFiles) >> (e ^. extension)

-- | All the files with all the extensions under a directory.
subDirExtensions ∷ ∀ (f ∷ * → *). Foldable f ⇒ RelDir → f RelFile → Maybe Pattern
subDirExtensions r es =
    let
        l1 = maybeToList $ dirExtensions r es
        lN = foldMapOf folded ((:[]) ∘ subDirExtension r) es
    in
        disjunction $ concatOf traversed [l1, lN]

-- | All the files in all the directories with the extensions.
subDirsExtensions ∷ ∀ (f ∷ * → *) (g ∷ * → *). (Foldable f, Foldable g) ⇒ f RelFile → g RelDir → Maybe Pattern
subDirsExtensions es =
    disjunction ∘ foldMapOf folded (maybeToList ∘ flip subDirExtensions es)

-- | 'subDirExtensions' when you know there's at least one.
subDirsExtensions' ∷ ∀ (f ∷ * → *) (g ∷ * → *). (Foldable f, Foldable g) ⇒ f RelFile → g RelDir → Pattern
subDirsExtensions' es ds = subDirsExtensions es ds ^?! _Just
